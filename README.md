# MazeRunner
 Bienvenu/e à notre jeu MazeRunner.
 Pour pouvoir jouer veuillez tout d'abord cloner le projet dans votre ordinateur à l'aide la commande 'git clone' suivie de la clé ssh (ou https comme vous le preferez) dans votre terminal.
 Ensuite veuillez utiliser la commande 'cd mazerunner/main' pour vous deplacer vers le projet.
 Puis lancez la compilation du jeu à l'aide de la commande 'make'.
 Finalement il ne reste qu'a lancer le jeu avec la commande './exe' et profiter d'un bon moment.

 Les règles du jeu sont plutôt simples:
 Après avoir lancer le jeu, saisissez votre nom et choisissez une difficulté:
 1 pour un niveau facil (avec 3 vies)
 2 pour un niveau moyen (avec 2 vies)
 3 pour un niveau avancé (avec 1 vie)
 et 4 pour un niveau géneré aléatoirement.
 Une fois le niveau corréctement choisi, le labyrinth s'affiche sous vos yeux.
 Pour avancer vous devez vous en servir des touches ZQSD.
 Attention! Il faut appuyer sur la touche 'Enter' après la saisie de la direction sinon elle ne sera pas prise en compte! 
 Vous pouvez vous déplacer plusieurs quand d'un seul coup par exemple pour avancer 3 cases vers le haut, vous pouvez ecrire 'zzz' puis appuyer sur enter.
 Finalement chaque labyrinth est different et il y a des pièges (ainsi que des potions) que vous découvrirez au fur et au mésure.
 Faites attention à vous! 