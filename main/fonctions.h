#ifndef __FONCTIONS__H
#define __FONCTIONS__H

typedef struct Pos
{
    int ligne;
    int colonne;
} Pos;

typedef struct Joueur
{
    char *nom;
    int vie;
    Pos position;
} Joueur;

typedef struct Labyrinth
{
    int **tab;
    int lignes;
    int colonnes;
    Pos entree;
    Pos sortie;
} Labyrinth;

typedef struct Jeu
{
    Labyrinth lab;
    Joueur joueur;
    int diff;
    int en_cours;
} Jeu;

typedef struct Noeud
{
    Pos valeur;
    struct Noeud *sah;
    struct Noeud *sab;
    struct Noeud *sag;
    struct Noeud *sad;
    int precedent; // 1 Gauche 2 Haut 3 Droite 4 Bas

} Noeud;

typedef struct Noeud* Arbre;

/* Auteur :  Raphael  */
/* Date :    28 décembre */
/* Résumé :  compteur de ligne */
/* Entrée(s) : un entier qui représente la difficulté*/
/* Sortie(s) :  retourne le nb de lignes  */
int countRow(int diff);

/* Auteur :  Ewen  */
/* Date :    28 décembre */
/* Résumé :  compteur de colonne */
/* Entrée(s) : un entier qui représente la difficulté*/
/* Sortie(s) :  retourne le nb de colonnes  */
int countColumn(int diff);

/* Auteur :  Abdelhamid  */
/* Date :    28 décembre */
/* Résumé :  lecture du fichier csv correpondant au labyrinthe */
/* Entrée(s) : un entier qui représente la difficulté*/
/* Sortie(s) :  retourne le labyrinthe  */
Labyrinth creerLab(int diff);

/* Auteur :  Ewen  */
/* Date :    3 janvier */
/* Résumé : affichage du labyrinthe */
/* Entrée(s) : le labyrinthe et la position du joueur*/
void afficherLab(Labyrinth lab, Pos pos);

/* Auteur : Abdelhamid  */
/* Date : 4janvier */
/* Résumé : initialisation du jeu et choix de difficulté */
/* Sortie(s) :  retourne le jeu */
Jeu creerJeu();

/* Auteur : Raphael  */
/* Date : 4janvier */
/* Résumé : initialisation de l'arbre */
/* Entrée(s) : position du joueur et case précédente*/
/* Sortie(s) :  retourne l'arbre  */
Arbre creerArbre(Pos position, int precedent);

/* Auteur : Raphael  */
/* Date : 6 janvier */
/* Résumé :  création de l'arbre correspondant au labyrinthe */
/* Entrée(s) : le labyrinthe et l'arbre*/
/* Sortie(s) :  retourne l'arbre  */
Arbre creerChemins(Labyrinth lab, Arbre a);

/* Auteur : Ewen  */
/* Date : 6 janvier */
/* Résumé : affichage de l'arbre */
/* Entrée(s) : l'arbre*/
void afficherArbre(Arbre a);

/* Auteur :  Abdelhamid  */
/* Date : 9 janvier */
/* Résumé : donne la taille de l'arbre */
/* Entrée(s) : l'arbre*/
/* Sortie(s) : la taille de l'arbre  */
int taille(Arbre a);

/* Auteur :  Abdelhamid  */
/* Date : 10 janvier */
/* Résumé : permet au joueur de se déplacer dans le labyrinthe */
/* Entrée(s) : le jeu*/
void movePlayer(Jeu* jeu);

/* Auteur :  Ewen  */
/* Date : 13 janvier */
/* Résumé : genere aléatoirement une matrice (labyrinthe) */
/* Entrée(s) : la taille */
/* Sortie(s) : le labyrinthe  */
Labyrinth genererLab(int taille);

/* Auteur :  Abdelhamid  */
/* Date :    18 janvier */
/* Résumé : Ecrit la matrice passé en paramètre dans un fichier csv */
/* Entrée(s) : le labyrinthe */
void ecrireLab(Labyrinth lab);

/* Auteur :  Abdelhamid  */
/* Date : 19 janvier */
/* Résumé : affiche le nombre de vies du joueur */
/* Entrée(s) : le jeu */
void affichageVies(Jeu jeu);

/* Auteur :  Abdelhamid  */
/* Date : 19 janvier */
/* Résumé : libere tout l'espace loué */
/* Entrée(s) : le jeu */
void freeJeu(Jeu *jeu);

#endif