#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include "fonctions.h"
#define N 1000

void emptyBuffer()
{
	int c;
	while ((c = getchar()) != '\n' && c != EOF)
		;
}

int countRow(int diff)
{
	FILE *matrice = NULL;
	char ligne[N];
	int nbLignesLues = 0;

	if (diff == 0)
	{
		matrice = fopen("matriceFacile.csv", "r");
	}
	else if (diff == 1)
	{
		matrice = fopen("matriceMoyenne.csv", "r");
	}
	else if (diff == 2)
	{
		matrice = fopen("matriceDifficile.csv", "r");
	}
	else
	{
		Labyrinth ale = genererLab(15);
		ecrireLab(ale);
		matrice = fopen("matrice.csv", "r");
	}

	if (matrice == NULL)
	{
		printf("Erreur ouverture fichier \n");
	}
	else
	{
		while (fgets(ligne, N, matrice) != NULL)
		{
			nbLignesLues++;
		}
		fclose(matrice);
	}
	return nbLignesLues;
}

int countColumn(int diff)
{
	int tmp = 1;
	FILE *matrice = NULL;
	char ligne[N];
	int nbColonnesLues = 0;
	int nbLignesLues = 0;
	char *laLigne;
	laLigne = malloc(sizeof(char) * N);
	if (diff == 0)
	{
		matrice = fopen("matriceFacile.csv", "r");
	}
	else if (diff == 1)
	{
		matrice = fopen("matriceMoyenne.csv", "r");
	}
	else if (diff == 2)
	{
		matrice = fopen("matriceDifficile.csv", "r");
	}
	else
	{
		Labyrinth ale = genererLab(15);
		ecrireLab(ale);
		matrice = fopen("matrice.csv", "r");
	}

	if (matrice == NULL)
	{
		printf("Erreur ouverture fichier \n");
	}
	else
	{
		while (fgets(ligne, N, matrice) != NULL && tmp)
		{
			laLigne = strtok(ligne, ";");
			while (laLigne != NULL)
			{
				laLigne = strtok(NULL, ";");
				nbColonnesLues++;
			}
			if (nbLignesLues == 2)
			{
				tmp = 0;
			}
			else
			{
				nbColonnesLues = 0;
				nbLignesLues++;
			}
		}
		fclose(matrice);
	}
	return nbColonnesLues;
}

Labyrinth creerLab(int diff)
{
	Labyrinth lab;

	lab.tab = malloc(sizeof(int *) * (countRow(diff) - 2));
	for (int i = 0; i < countRow(diff) - 2; i++)
	{
		lab.tab[i] = malloc(sizeof(int) * countColumn(diff));
	}
	lab.colonnes = countColumn(diff);
	lab.lignes = countRow(diff) - 2;
	FILE *matrice = NULL;
	char ligne[N];
	int nbLignesLues = 0;
	int nbColonnesLues = 0;
	char *laLigne;
	laLigne = malloc(sizeof(char) * N);
	if (diff == 0)
	{
		matrice = fopen("matriceFacile.csv", "r");
	}
	else if (diff == 1)
	{
		matrice = fopen("matriceMoyenne.csv", "r");
	}
	else if (diff == 2)
	{
		matrice = fopen("matriceDifficile.csv", "r");
	}
	else
	{
		Labyrinth ale = genererLab(15);
		ecrireLab(ale);
		matrice = fopen("matrice.csv", "r");
	}
	if (matrice == NULL)
	{
		printf("Erreur ouverture fichier \n");
	}
	else
	{
		while (fgets(ligne, N, matrice) != NULL)
		{
			laLigne = strtok(ligne, ";");
			while (laLigne != NULL)
			{
				if (nbLignesLues == 0)
				{
					if (nbColonnesLues == 0)
					{
						lab.entree.ligne = atoi(laLigne);
					}
					else
					{
						lab.entree.colonne = atoi(laLigne);
					}
				}
				else if (nbLignesLues == 1)
				{
					if (nbColonnesLues == 0)
					{
						lab.sortie.ligne = atoi(laLigne);
					}
					else
					{
						lab.sortie.colonne = atoi(laLigne);
					}
				}
				else if (nbLignesLues >= 2)
				{
					lab.tab[nbLignesLues - 2][nbColonnesLues] = atoi(laLigne);
				}
				laLigne = strtok(NULL, ";");
				nbColonnesLues++;
			}
			nbColonnesLues = 0;
			nbLignesLues++;
		}
		fclose(matrice);
	}
	return lab;
}

void afficherLab(Labyrinth lab, Pos pos)
{
	for (int i = 0; i < lab.lignes; i++)
	{
		for (int j = 0; j < lab.colonnes; j++)
		{
			if (i == pos.ligne && j == pos.colonne)
			{
				printf(" 🤖");
			}
			else if (lab.tab[i][j] == -1)
			{
				printf(" 🧱");
			}
			else if (lab.tab[i][j] == 1 || lab.tab[i][j] == 2)
			{
				printf(" 🚪");
			}
			else
			{
				printf("   ");
			}
		}
		printf("\n");
	}
	printf("\n\n");
}

Jeu creerJeu()
{
	Jeu jeu;
	int tmp;
	int typetmp;

	jeu.joueur.nom = malloc(sizeof(char) * 25);
	printf("Bienvenu au MazeRunner, veuillez saisir votre nom: ");
	scanf("%s", jeu.joueur.nom);
	do
	{
		printf("\nVeuillez choisir une difficulté de 1 à 4: ");
		typetmp = scanf("%d", &tmp);
		emptyBuffer();
	} while (typetmp == 0 || (tmp < 1 || tmp > 4));
	jeu.diff = tmp;
	jeu.lab = creerLab(tmp - 1);
	jeu.joueur.position.ligne = jeu.lab.entree.ligne;
	jeu.joueur.position.colonne = jeu.lab.entree.colonne;
	switch (tmp)
	{
	case 1:
		jeu.joueur.vie = 3;
		break;

	case 2:
		jeu.joueur.vie = 2;
		break;

	default:
		jeu.joueur.vie = 1;
		break;
	}
	jeu.en_cours = 1;

	return jeu;
}

Arbre creerArbre(Pos position, int precedent)
{
	Arbre a;
	a = malloc(sizeof(Noeud));
	a->valeur = position;
	a->sag = NULL;
	a->sad = NULL;
	a->sah = NULL;
	a->sab = NULL;
	a->precedent = precedent;
	return a;
}

Arbre creerChemins(Labyrinth lab, Arbre a)
{
	Pos tmp;
	if (a == NULL)
	{
		return a;
	}
	if ((a->valeur.colonne > 0) && (a->precedent != 1) && ((lab.tab[a->valeur.ligne][a->valeur.colonne - 1] == 0) || (lab.tab[a->valeur.ligne][a->valeur.colonne - 1] == 2)))
	{ // Gauche
		tmp.ligne = a->valeur.ligne;
		tmp.colonne = a->valeur.colonne - 1;
		a->sag = creerArbre(tmp, 3);
		a->sag = creerChemins(lab, a->sag);
	}
	if ((a->valeur.colonne + 1 < lab.colonnes) && (a->precedent != 3) && ((lab.tab[a->valeur.ligne][a->valeur.colonne + 1] == 0) || (lab.tab[a->valeur.ligne][a->valeur.colonne + 1] == 2)))
	{ // Droite
		tmp.ligne = a->valeur.ligne;
		tmp.colonne = a->valeur.colonne + 1;
		a->sad = creerArbre(tmp, 1);
		a->sad = creerChemins(lab, a->sad);
	}
	if ((a->valeur.ligne > 0) && (a->precedent != 2) && ((lab.tab[a->valeur.ligne - 1][a->valeur.colonne] == 0) || (lab.tab[a->valeur.ligne - 1][a->valeur.colonne] == 2)))
	{ // Haut
		tmp.ligne = a->valeur.ligne - 1;
		tmp.colonne = a->valeur.colonne;
		a->sah = creerArbre(tmp, 4);
		a->sah = creerChemins(lab, a->sah);
	}
	if ((a->valeur.ligne + 1 < lab.lignes) && (a->precedent != 4) && ((lab.tab[a->valeur.ligne + 1][a->valeur.colonne] == 0) || (lab.tab[a->valeur.ligne + 1][a->valeur.colonne] == 2)))
	{ // Bas
		tmp.ligne = a->valeur.ligne + 1;
		tmp.colonne = a->valeur.colonne;
		a->sab = creerArbre(tmp, 2);
		a->sab = creerChemins(lab, a->sab);
	}

	return a;
}

void afficherArbre(Arbre a)
{
	if (a != NULL)
	{
		printf("[%d ; %d]\n", a->valeur.ligne, a->valeur.colonne);
		afficherArbre(a->sag);
		afficherArbre(a->sah);
		afficherArbre(a->sad);
		afficherArbre(a->sab);
	}
}

int taille(Arbre a)
{
	if (a != NULL)
		return 1 + taille(a->sag) + taille(a->sad) + taille(a->sah) + taille(a->sab);
	else
		return 0;
}

void movePlayer(Jeu *jeu)
{
	char direction;
	int typetmp;
	do
	{
		printf("Veuillez utiliser les touches Z ( haut) S (bas) Q (gauche) et D (droite) pour choisir votre direction: ");
		typetmp = scanf(" %c", &direction);

	} while (typetmp == 0 || (direction != 'z' && direction != 'q' && direction != 's' && direction != 'd'));
	if (direction == 'z')
	{
		if (jeu->joueur.position.ligne > 0 && (jeu->lab.tab[jeu->joueur.position.ligne - 1][jeu->joueur.position.colonne] >= 0))
		{
			jeu->joueur.position.ligne--;
		}
		else
		{
			printf("La direction choisie est un mur ! ");
		}
	}
	if (direction == 'q')
	{
		if (jeu->joueur.position.colonne > 0 && (jeu->lab.tab[jeu->joueur.position.ligne][jeu->joueur.position.colonne - 1] >= 0))
		{
			jeu->joueur.position.colonne--;
		}
		else
		{
			printf("La direction choisie est un mur ! ");
		}
	}
	if (direction == 's')
	{
		if (jeu->joueur.position.ligne < jeu->lab.lignes - 1 && (jeu->lab.tab[jeu->joueur.position.ligne + 1][jeu->joueur.position.colonne] >= 0))
		{
			jeu->joueur.position.ligne++;
		}
		else
		{
			printf("La direction choisie est un mur ! ");
		}
	}
	if (direction == 'd')
	{
		if (jeu->joueur.position.colonne < jeu->lab.colonnes - 1 && (jeu->lab.tab[jeu->joueur.position.ligne][jeu->joueur.position.colonne + 1] >= 0))
		{
			jeu->joueur.position.colonne++;
		}
		else
		{
			printf("La direction choisie est un mur ! ");
		}
	}

	if (jeu->lab.tab[jeu->joueur.position.ligne][jeu->joueur.position.colonne] == 3)
	{
		jeu->joueur.position.ligne = jeu->lab.entree.ligne;
		jeu->joueur.position.colonne = jeu->lab.entree.colonne;
	}
	else if (jeu->lab.tab[jeu->joueur.position.ligne][jeu->joueur.position.colonne] == 4)
	{
		printf("Vous êtes tombé dans un piège, vous avez perdu une vie.\n");
		sleep(3);
		jeu->joueur.vie--;
	}
	else if (jeu->lab.tab[jeu->joueur.position.ligne][jeu->joueur.position.colonne] == 5)
	{
		printf("Vous etes tombé dans le piège Ultime, vous etes foutu");
		sleep(3);
		jeu->joueur.vie = 0;
	}
	else if (jeu->lab.tab[jeu->joueur.position.ligne][jeu->joueur.position.colonne] == 6)
	{
		printf("Bonus ! Vous avez gagné une vie \n");
		sleep(3);
		jeu->joueur.vie++;
	}
	else if (jeu->lab.tab[jeu->joueur.position.ligne][jeu->joueur.position.colonne] == 7)
	{
		printf("Bonus ultime ! Vous êtes redirigé vers la fin, A vous de jouer ! \n");
		sleep(3);
		switch (jeu->diff)
		{
		case 3:
			jeu->joueur.position.ligne = 3;
			jeu->joueur.position.colonne = 14;
			break;

		case 2:
			jeu->joueur.position.ligne = 13;
			jeu->joueur.position.colonne = 8;
			break;

		default:
			jeu->joueur.position.ligne = 13;
			jeu->joueur.position.colonne = 8;
			break;
		}
	}
}

Labyrinth genererLab(int taille)
{
	Labyrinth lab;
	lab.tab = malloc(sizeof(int *) * taille);

	for (int i = 0; i < taille; i++)
	{
		lab.tab[i] = malloc(sizeof(int) * taille);
	}

	for (int i = 0; i < taille; i++)
	{
		for (int j = 0; j < taille; j++)
		{
			lab.tab[i][j] = -1;
		}
	}

	int tmp = rand() % 4;

	do
	{
		switch (tmp)
		{
		case 0:
			lab.entree.ligne = rand() % taille;
			lab.entree.colonne = 0;
			lab.sortie.ligne = rand() % taille;
			lab.sortie.colonne = taille - 1;
			break;
		case 1:
			lab.entree.ligne = 0;
			lab.entree.colonne = rand() % taille;
			lab.sortie.ligne = taille - 1;
			lab.sortie.colonne = rand() % taille;
			break;
		case 2:
			lab.entree.ligne = rand() % taille;
			lab.entree.colonne = taille - 1;
			lab.sortie.ligne = rand() % taille;
			lab.sortie.colonne = 0;
			break;
		default:
			lab.entree.ligne = taille - 1;
			lab.entree.colonne = rand() % taille;
			lab.sortie.ligne = 0;
			lab.sortie.colonne = rand() % taille;
			break;
		}
	} while ((lab.entree.ligne == 0 && lab.entree.colonne == 0) || (lab.sortie.ligne == 0 && lab.sortie.colonne == 0));

	lab.lignes = taille;
	lab.colonnes = taille;
	lab.tab[lab.entree.ligne][lab.entree.colonne] = 1;
	lab.tab[lab.sortie.ligne][lab.sortie.colonne] = 2;

	// Generate paths
	int currentRow = lab.entree.ligne;
	int currentCol = lab.entree.colonne;

	while (currentRow != lab.sortie.ligne || currentCol != lab.sortie.colonne)
	{
		int direction;
		do
		{
			direction = rand() % 4;
		} while (
			(direction == 0 && currentRow == 0) ||
			(direction == 1 && currentRow == taille - 1) ||
			(direction == 2 && currentCol == 0) ||
			(direction == 3 && currentCol == taille - 1));

		switch (direction)
		{
		case 0: // Move up
			currentRow--;
			break;
		case 1: // Move down
			currentRow++;

			break;
		case 2: // Move left
			currentCol--;

			break;
		case 3: // Move right
			currentCol++;

			break;
		}

		// Check if the next cell is unvisited
		if (lab.tab[currentRow][currentCol] == -1)
		{
			if ((currentRow > 0 && lab.tab[currentRow - 1][currentCol] == -1) || (currentRow < taille - 1 && lab.tab[currentRow + 1][currentCol] == -1) || (currentCol < taille - 1 && lab.tab[currentRow][currentCol + 1] == -1) || (currentCol > 0 && lab.tab[currentRow][currentCol - 1] == -1))
			{
				lab.tab[currentRow][currentCol] = 0;
			}
		}
	}

	return lab;
}

void ecrireLab(Labyrinth lab)
{
	FILE *file = fopen("matrice.csv", "w");
	if (file == NULL)
	{
		printf("Error opening the file.\n");
		return;
	}

	fprintf(file, "%d", lab.entree.ligne);
	fprintf(file, ";");
	fprintf(file, "%d", lab.entree.colonne);
	fprintf(file, "\n");
	fprintf(file, "%d", lab.sortie.ligne);
	fprintf(file, ";");
	fprintf(file, "%d", lab.sortie.colonne);
	fprintf(file, "\n");

	for (int i = 2; i < lab.lignes + 2; ++i)
	{
		for (int j = 0; j < lab.colonnes; ++j)
		{
			fprintf(file, "%d", lab.tab[i - 2][j]);
			// Add a comma after each element, except for the last one in the row
			if (j < lab.colonnes - 1)
			{
				fprintf(file, ";");
			}
		}
		fprintf(file, "\n"); // Move to the next line after each row
	}
	fclose(file);
}

void affichageVies(Jeu jeu)
{
	for (int i = 0; i < jeu.joueur.vie; i++)
	{
		printf(" ❤️ ");
	}
	printf("\n\n");
}

void freeJeu(Jeu *jeu)
{
	free(jeu->joueur.nom);
	for (int i = 0; i < jeu->lab.lignes; i++)
	{
		free(jeu->lab.tab[i]);
	}
	free(jeu->lab.tab);
}