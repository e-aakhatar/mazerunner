#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include "fonctions.h"

int main()
{
    char tmp;
    srand(time(NULL));

    Jeu jeu = creerJeu();
    printf("\n");

    while (jeu.en_cours)
    {
        system("clear");
        affichageVies(jeu);
        afficherLab(jeu.lab, jeu.joueur.position);
        movePlayer(&jeu);
        if (jeu.joueur.vie == 0)
        {
            printf("Vous n'avez plus de vies! Eliminé.\n");
            if (jeu.diff != 4)
            {
                Arbre chemins;
                chemins = creerArbre(jeu.lab.entree, 0);
                chemins = creerChemins(jeu.lab, chemins);
                printf("Arbre: \n");
                afficherArbre(chemins);
            }
            printf("Voulez vous rejouer ? [Y/N]: ");
            scanf(" %c", &tmp);
            fflush(stdin);

            if (tmp == 'y')
            {
                system("clear");
                freeJeu(&jeu);
                jeu = creerJeu();
            }
            else
            {
                jeu.en_cours = 0;
            }
        }
        else if (jeu.joueur.position.ligne == jeu.lab.sortie.ligne && jeu.joueur.position.colonne == jeu.lab.sortie.colonne)
        {
            printf("Vous avez atteint la sortie du labyrinth, félicitation %s! \n", jeu.joueur.nom);
            if (jeu.diff != 4)
            {
                Arbre chemins;
                chemins = creerArbre(jeu.lab.entree, 0);
                chemins = creerChemins(jeu.lab, chemins);
                printf("Arbre des chemins: \n");
                afficherArbre(chemins);
            }
            printf("Voulez vous rejouer ? [Y/N]: ");
            scanf(" %c", &tmp);
            fflush(stdin);

            if (tmp == 'y')
            {
                system("clear");
                freeJeu(&jeu);
                jeu = creerJeu();
            }
            else
            {
                jeu.en_cours = 0;
            }
        }
    }

    return 0;
}